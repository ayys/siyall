#include <stdlib.h>

#ifndef SIYALL_H
#define SIYALL_H

struct siyall;			/* Linked List */
struct siyall_item;		/* Item in Linked List */

typedef struct siyall* siyall;
typedef struct siyall_item* siyall_item;

/* create a new instance of Linked List */
siyall create_siyall();
/* duplicate the whole linked list into a new list */
siyall copy_siyall(siyall);
/* destroy the Linked List */
void destroy_siyall(siyall);

/* get the first item in the linked list */
siyall_item siyall_get_first_item(siyall);
/* given an item, get the next item in the linked list */
siyall_item siyall_get_next_item(siyall_item);
/* Get the total number of items in the linked list */
long int siyall_get_length(siyall);

/* add a new item to the linked list */
void siyall_add_item(siyall);
/* duplicate an item into a new Item  */
siyall_item siyall_copy_item(siyall_item);

#endif
